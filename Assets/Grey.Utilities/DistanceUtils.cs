// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System;
using UnityEngine;

namespace Grey.Utilities
{
    public static class DistanceUtils
    {
        private const float Centimeter = 0.01f;
        private const float Kilometer = 1000f;

        private const float Inch = 0.025f;
        private const float Foot = 0.305f;
        private const float Yard = 0.914f;
        private const float Mile = 1609.344f;

        public enum ReadoutType
        {
            Metric = 0,
            Imperial = 1,
        }

        public static ReadoutType Type = ReadoutType.Metric;

        public static string ToReadoutString(float meters, bool roundToInt = false)
        {
            return ToReadoutString(meters, Type, roundToInt);
        }

        public static string ToReadoutString(float meters, ReadoutType type, bool roundToInt = false)
        {
            switch (type)
            {
                case ReadoutType.Metric: return meters.ToMetric(roundToInt);
                case ReadoutType.Imperial: return meters.ToImperial(roundToInt);
                default: throw new ArgumentOutOfRangeException();
            }
        }

        private static string ToMetric(this float meters, bool roundToInt)
        {
            if (meters > Kilometer)
            {
                var kms = meters / Kilometer;
                return string.Format("{0} km", roundToInt ? kms.ToString("N0") : kms.ToString("N2"));
            }

            if (meters > 1f)
            {
                return string.Format("{0} m", roundToInt ? meters.ToString("N0") : meters.ToString("N2"));
            }

            var cms = meters / Centimeter;
            return string.Format("{0} cm", roundToInt ? cms.ToString("N0") : cms.ToString("N2"));
        }

        private static string ToImperial(this float meters, bool roundToInt)
        {
            if (meters > Mile)
            {
                var miles = meters / Mile;
                return string.Format("{0} {1}",
                                     roundToInt ? miles.ToString("N0") : miles.ToString("N2"),
                                     Mathf.Abs(miles - 1) > Mathf.Epsilon ? "miles" : "mile");
            }

            if (meters > Yard)
            {
                var yards = meters / Yard;
                return string.Format("{0} {1}",
                                     roundToInt ? yards.ToString("N0") : yards.ToString("N2"),
                                     Mathf.Abs(yards - 1) > Mathf.Epsilon ? "yards" : "yard");
            }

            if (meters > Foot)
            {
                var feet = meters / Foot;
                return string.Format("{0} {1}",
                                     roundToInt ? feet.ToString("N0") : feet.ToString("N2"),
                                     Mathf.Abs(feet - 1) > Mathf.Epsilon ? "feet" : "foot");
            }

            var inches = meters / Inch;
            return string.Format("{0} {1}",
                                 roundToInt ? inches.ToString("N0") : inches.ToString("N2"),
                                 Mathf.Abs(inches - 1) > Mathf.Epsilon ? "inches" : "inch");
        }
    }
}
