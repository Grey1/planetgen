// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System;
using System.Globalization;
using System.Text;
using UnityEngine;

namespace Grey.Utilities
{
    public static class StringTools
    {
        //TODO: this is for when using I2 localization plugin:
        /*public static string TranslateText(string parameter)
		{
			var text = I2.Loc.ScriptLocalization.Get(parameter);
			return !string.IsNullOrEmpty(text) ? text : string.Format("Localization data is missing for \"{0}\"", parameter);
		}*/

        #region Coloured

        // rewritten to do all work in a single string.format call, rather than across three or four - saves on garbage generation at the expense of easy code updates.
        public static string Coloured(this string s, Color32 c)
        {
            //return string.Format("{0}{1}</color>", c.ToHTML(), s);
            return string.Format("<color=#{0:x2}{1:x2}{2:x2}{3:x2}>{4}</color>", c.r, c.g, c.b, c.a, s);
        }

        public static string Coloured<T>(this T t, Color32 c)
        {
            //return t.ToString().Coloured(c);
            return string.Format("<color=#{0:x2}{1:x2}{2:x2}{3:x2}>{4}</color>", c.r, c.g, c.b, c.a, t);
        }

        public static string Coloured<T>(this T t, Color32 c, string format, IFormatProvider formatProvider = null) where T : IFormattable
        {
            //return t.ToString(format, formatProvider).Coloured(c);
            return string.Format("<color=#{0:x2}{1:x2}{2:x2}{3:x2}>{4}</color>", c.r, c.g, c.b, c.a, t.ToString(format, formatProvider));
        }

        #endregion

        #region Colored

        // Helpers for the English-impaired ^_^

        public static string Colored(this string s, Color c)
        {
            return s.Coloured(c);
        }

        public static string Colored<T>(this T t, Color c)
        {
            return t.Coloured(c);
        }

        public static string Colored<T>(this T t, Color c, string format, IFormatProvider formatProvider = null) where T : IFormattable
        {
            return t.Coloured(c, format, formatProvider);
        }

        #endregion

        #region Gradient

        public static string Gradient<T>(this T t, Color32 gradientStart, Color32 gradientEnd)
        {
            var chars = t.ToString().ToCharArray();
            return CalculateGradient(chars, gradientStart, gradientEnd);
        }

        public static string Gradient<T>(this T t, Color32 gradientStart, Color32 gradientEnd, string format, IFormatProvider formatProvider = null) where T : IFormattable
        {
            var chars = t.ToString(format, formatProvider).ToCharArray();
            return CalculateGradient(chars, gradientStart, gradientEnd);
        }

        private static string CalculateGradient(char[] chars, Color32 start, Color32 end)
        {
            var sb = new StringBuilder();
            var count = chars.Length;

            for (var i = 0; i < chars.Length; i++)
            {
                var c = Color32.Lerp(start, end, Mathf.Clamp01((float)i / count));
                sb.AppendFormat("<color=#{0:x2}{1:x2}{2:x2}{3:x2}>{4}</color>", c.r, c.g, c.b, c.a, chars[i]);
            }

            return sb.ToString();
        }

        #endregion

        #region LineBreak

        public static string LineBreak(this string s)
        {
            return string.Concat(s, "\n");
        }

        public static string LineBreak<T>(this T t)
        {
            return string.Format("{0}\n", t);
        }

        public static string LineBreak<T>(this T t, string format, IFormatProvider formatProvider = null) where T : IFormattable
        {
            return t.ToString(format, formatProvider).LineBreak();
        }

        #endregion LineBreak

        #region Italics

        public static string Italics(this string s)
        {
            return string.Format("<i>{0}</i>", s);
        }

        public static string Italics<T>(this T t)
        {
            return string.Format("<i>{0}</i>", t);
        }

        public static string Italics<T>(this T t, string format, IFormatProvider formatProvider = null) where T : IFormattable
        {
            return t.ToString(format, formatProvider).Bold();
        }

        #endregion Italics

        #region Bold

        public static string Bold(this string s)
        {
            return string.Format("<b>{0}</b>", s);
        }

        public static string Bold<T>(this T t)
        {
            return string.Format("<b>{0}</b>", t);
        }

        public static string Bold<T>(this T t, string format, IFormatProvider formatProvider = null) where T : IFormattable
        {
            return t.ToString(format, formatProvider).Bold();
        }

        #endregion Bold

        #region SmallCaps

        public static string SmallCaps(string inputString, int tallFontSize = 18, int smallFontSize = 14)
        {
            if (inputString.Length > 0)
            {
                var s = inputString.ToUpper();
                return string.Format("<size={0}>{1}</size><size={2}>{3}</size>", tallFontSize, s[0], smallFontSize, s.Substring(1));
            }

            return string.Empty;
        }

        public static string SmallCaps<T>(this T t, int tallFontSize = 18, int smallFontSize = 14)
        {
            return t.ToString().SmallCaps(tallFontSize, smallFontSize);
        }

        public static string SmallCaps<T>(this T t, int tallFontSize = 18, int smallFontSize = 14, string format = "", IFormatProvider formatProvider = null) where T : IFormattable
        {
            return t.ToString(format, formatProvider).SmallCaps(tallFontSize, smallFontSize);
        }

        #endregion SmallCaps

        #region HTML

        public static string ToHTML(this Color c)
        {
            return ((Color32)c).ToHTML();
        }

        public static string ToHTML(this Color32 c)
        {
            //return string.Format("<color=#{0}>", c.ToHex());
            return string.Format("<color=#{0:x2}{1:x2}{2:x2}{3:x2}>", c.r, c.g, c.b, c.a);
        }

        #endregion HTML

        #region HEX

        public static string ToHex(this Color c)
        {
            return ((Color32)c).ToHex();
        }

        public static string ToHex(this Color32 c)
        {
            return string.Format("{0:x2}{1:x2}{2:x2}{3:x2}", c.r, c.g, c.b, c.a);
        }

        /// <summary>
        /// var red = "#FF0000"; // accepts # at beginning of string
        /// var green = "00FF00"; // accepts RGB
        /// var blue = "0000FF00"; // accepts RGBA
        /// var white = "#FFFFFFFF"; // accepts all of the above
        /// </summary>
        public static Color FromHex(string hex, bool ignoreAlpha = false)
        {
            if (hex.Length < 6)
                throw new ArgumentException("Hex string must be at least 6 characters long.");

            var i = 0;

            if (hex[0] == '#')
                i++;

            short r, g, b, a;

            if (!short.TryParse(hex.Substring(i + 0, 2), NumberStyles.HexNumber, null, out r))
                throw new ArgumentException(string.Format("Unable to parse hexadecimal output from substring: {0} || full string: {1}", hex.Substring(i + 0, 2), hex));

            if (!short.TryParse(hex.Substring(i + 2, 2), NumberStyles.HexNumber, null, out g))
                throw new ArgumentException(string.Format("Unable to parse hexadecimal output from substring: {0} || full string: {1}", hex.Substring(i + 2, 2), hex));

            if (!short.TryParse(hex.Substring(i + 4, 2), NumberStyles.HexNumber, null, out b))
                throw new ArgumentException(string.Format("Unable to parse hexadecimal output from substring: {0} || full string: {1}", hex.Substring(i + 4, 2), hex));

            var fr = r / 255.0f;
            var fg = g / 255.0f;
            var fb = b / 255.0f;

            // interpret RGB hex codes
            if (ignoreAlpha || hex.Length < 8)
                return new Color(fr, fg, fb);

            // interpret RGBA hex codes
            if (!short.TryParse(hex.Substring(i + 6, 2), NumberStyles.HexNumber, null, out a))
                throw new ArgumentException(string.Format("Unable to parse hexadecimal output from substring: {0} || full string: {1}", hex.Substring(i + 6, 2), hex));

            return new Color(fr, fg, fb, a / 255.0f);
        }

        #endregion HEX
    }
}