// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

//#define DISABLED
//#define USING_UNITY_PROFILER

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Grey.Utilities
{
    public class Profile
    {
#if UNITY_EDITOR && !DISABLED && USING_UNITY_PROFILER
        public void AddCounter(string tag, int count) { }

        public void StartProfile(string tag)
        {
            Profiler.BeginSample(tag);
        }

        public void EndProfile(string tag)
        {
            Profiler.EndSample();
        }
        public void Reset() { }
        public void PrintResults(string header = "Profile results") { }
        public string ResultString(string header = "Profile results") { return string.Empty; }

#elif UNITY_EDITOR && !DISABLED

		public struct ProfilePoint
		{
			public DateTime LastRecorded;
			public TimeSpan TotalTime;
			public int TotalCalls;
		}

		private readonly List<string> _messages = new List<string>();
		private readonly Dictionary<string, ProfilePoint> _profiles = new Dictionary<string, ProfilePoint>();
		private readonly Dictionary<string, int> _counters = new Dictionary<string, int>();
		private readonly TimeSpan _maxIdle = TimeSpan.FromSeconds(10);
		private DateTime startTime = DateTime.UtcNow;
		private DateTime _startTime;
		private Stopwatch _stopWatch = null;

		public DateTime UtcNow
		{
			get
			{
				if (_stopWatch == null || startTime.Add(_maxIdle) < DateTime.UtcNow)
				{
					_startTime = DateTime.UtcNow;
					_stopWatch = Stopwatch.StartNew();
				}
				return _startTime.AddTicks(_stopWatch.Elapsed.Ticks);
			}
		}

		public void AddMessage(string flag)
		{
			_messages.Add(flag);
		}

		public void AddCounter(string tag, int count)
		{
			int currentCount = 0;
			if (_counters.TryGetValue(tag, out currentCount))
				_counters[tag] = currentCount + count;
			else
				_counters.Add(tag, count);
		}

		public void StartProfile(string tag)
		{
			ProfilePoint point;
			_profiles.TryGetValue(tag, out point);
			point.LastRecorded = UtcNow;
			_profiles[tag] = point;
		}

		public void EndProfile(string tag)
		{
			if (!_profiles.ContainsKey(tag))
			{
				Debug.LogError(string.Format("Can only end profiling for a tag which has already been started (tag was \"{0}\")", tag));
				return;
			}
			var point = _profiles[tag];
			point.TotalTime += UtcNow - point.LastRecorded;
			++point.TotalCalls;
			_profiles[tag] = point;
		}

		public void Reset()
		{
			_profiles.Clear();
			_counters.Clear();
			_messages.Clear();
			startTime = DateTime.UtcNow;
		}

		public string ResultString(string header = "Profile Data")
		{
			var endTime = DateTime.UtcNow - startTime;
			var output = new StringBuilder();
			output.AppendFormat(" == <color=red>{0}</color>: {1:N2} seconds\n", header, endTime.TotalSeconds);
			output.Append("[Select For Profile Results]\n");

			if (_messages.Count > 0)
				output.AppendLine();
			for (int i = 0; i < _messages.Count; i++)
			{
				output.AppendLine(_messages[i]);
			}

			var c = _counters.ToList();
			for (int i = 0; i < c.Count; i++)
			{
				var pair = c[i];
				output.AppendFormat("\nCounter {0} = {1}", pair.Key.Coloured(Color.white), pair.Value.ToString().Coloured(Color.cyan));
			}

			if (c.Count > 0)
				output.AppendLine();

			var p = _profiles.ToList();
			for (int i = 0; i < p.Count; i++)
			{
				var pair = p[i];
				var totalCalls = pair.Value.TotalCalls;
				if (totalCalls < 1) continue;
				output.AppendFormat("\nProfile {0} took <color=white>{1:N3}</color> seconds ", pair.Key, pair.Value.TotalTime.TotalSeconds);
				output.AppendFormat("to complete over <color=yellow>{0:N0}</color> iteration{1}, ", totalCalls, totalCalls != 1 ? "s" : "");
				output.AppendFormat("averaging <color=green>{0:N4}</color> ms per call", pair.Value.TotalTime.TotalMilliseconds / totalCalls);
			}

			output.Append("\n============================\n");
			output.AppendFormat("\tTotal runtime: {0:N2} seconds\n", endTime.TotalSeconds);
			output.Append("============================\n");
			return output.ToString();
		}

		public void PrintResults(string header = "Profile results")
		{
			var s = ResultString(header);
			Debug.Log(s);
		}

#else
        public void AddCounter(string tag, int count) { }
        public void StartProfile(string tag) { }
        public void EndProfile(string tag) { }
        public void Reset() { }
        public void PrintResults(string header = "Profile results") { }
        public string ResultString(string header = "Profile results") { return string.Empty; }

#endif
    }
}