﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System;
using UnityEngine;

namespace Grey.Utilities.Noise
{
    [Serializable]
    public abstract class NoiseSettings
    {
        protected NoiseSettings()
        {
            Simplex = UseRandomSeed
                          ? new OpenSimplex()
                          : new OpenSimplex(Seed);
        }

        public long Seed = 1337;
        public bool UseRandomSeed = false;

        protected OpenSimplex Simplex;

        [Range(0f, 30f)] public float Scale = 0.15f;
        [Range(1, 7)] public int Octaves = 3;

        public abstract float GetFloat(Vector3 v);
    }

    [Serializable]
    public class SimplexSettings : NoiseSettings
    {
        [Range(0f, 2f)] public float Lacunarity = 0.666f;
        [Range(0f, 1f)] public float Persistence = 0.333f;
        [Range(0f, 2f)] public float Frequency = 1f;
        [Range(0f, 2f)] public float Amplitude = 1f;

        public override float GetFloat(Vector3 v)
        {
            return Simplex.GetFloat(v, Scale, Octaves, Persistence, Lacunarity, Frequency, Amplitude);
        }
    }

	[Serializable]
	public class BillowSettings : NoiseSettings
    {
		[Range(0f, 2f)] public float Lacunarity = 0.666f;
		[Range(0f, 1f)] public float Persistence = 0.333f;
		[Range(0f, 2f)] public float Frequency = 1f;

        public override float GetFloat(Vector3 v)
        {
            return Simplex.GetBillowFloat(v, Scale, Octaves, Persistence, Lacunarity, Frequency);
        }
    }

	[Serializable]
	public class RidgeSettings : NoiseSettings
    {
		[Range(1f, 2f)] public float Exponent = 1.0f;
		[Range(0f, 2f)] public float Lacunarity = 0.666f;
		[Range(0f, 2f)] public float Frequency = 1f;
		[Range(0f, 2f)] public float Offset = 1f;
		[Range(0f, 2f)] public float Gain = 2f;

        private double[] _ridgeWeights;

        public void GenerateRidgeWeights()
        {
            _ridgeWeights = NoiseExtensions.CalculateRidgeWeights(Octaves, Exponent, Lacunarity);
        }

        public override float GetFloat(Vector3 v)
        {
            return Simplex.GetRidgedFloat(v, ref _ridgeWeights, Scale, Octaves, Frequency, Lacunarity, Offset, Gain);
        }
    }
}