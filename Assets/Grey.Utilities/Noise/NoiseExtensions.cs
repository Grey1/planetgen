﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using Grey.Utilities.Extensions;
using System;
using UnityEngine;

namespace Grey.Utilities.Noise
{
    public static class NoiseExtensions
    {
        #region Billow OpenSimple

        // todo: v2 and v4 versions of these functions.

        public static float GetBillowFloat(this OpenSimplex os, Vector3 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0)
        {
            return (float) os.GetBillowDouble(v, scale, octaves, persistance, lacunarity, frequency);
        }

        public static double GetBillowDouble(this OpenSimplex os, Vector3 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0)
        {
            double noise = 0.0;

            double x = v.x / scale * frequency;
            double y = v.y / scale * frequency;
            double z = v.z / scale * frequency;

            var simplex = os;
            for (int i = 0; i < octaves; i++)
            {
                noise = simplex.Evaluate(x, y, z);
                noise = 2.0 * Math.Abs(noise) - 1.0;
                noise += noise * persistance;
                persistance *= persistance;
                x *= lacunarity;
                y *= lacunarity;
                z *= lacunarity;
            }
            noise += 0.5;

            return noise;
        }

        #endregion

        #region Ridged OpenSimplex

        // todo: v2 and v4 versions of these functions.

        public static float GetRidgedFloat(this OpenSimplex os, Vector3 v, ref double[] weights, double scale = 1.0, int octaves = 3, double frequency = 1.0, double lacunarity = 2.0, double offset = 1.0, double gain = 2.0)
        {
            return (float) os.GetRidgedDouble(v, ref weights, scale, octaves, frequency, lacunarity, offset, gain);
        }

        public static double GetRidgedDouble(this OpenSimplex os, Vector3 v, ref double[] weights, double scale = 1.0, int octaves = 3, double frequency = 1.0, double lacunarity = 2.0, double offset = 1.0, double gain = 2.0)
        {
            scale = Math.Max(scale, 0.00001);
            octaves = Math.Max(octaves, 1);

            double noise = 0;
            double x = v.x / scale * frequency;
            double y = v.y / scale * frequency;
            double z = v.z / scale * frequency;

            double weight = 1.0;

            var simplex = os;
            for (var i = 0; i < octaves; i++)
            {
                var n = simplex.Evaluate(x, y, z);
                n = offset - Math.Abs(n);
                n = (n * n) * weight;
                weight = MathExtensions.Clamp(0.0, 1.0, n * gain);

                noise += n * weights[i];

                x *= lacunarity;
                y *= lacunarity;
                z *= lacunarity;
            }

            return (noise * 1.25) - 1.0;
        }

        public static double[] CalculateRidgeWeights(int octaves, double exponent, double lacunarity)
        {
            var weights = new double[octaves];
            double frequency = 1.0;
            for (var i = 0; i < octaves; i++)
            {
                weights[i] = Math.Pow(frequency, -exponent);
                frequency *= lacunarity;
            }

            return weights;
        }

        #endregion

        #region OpenSimplex

        public static float GetFloat(this OpenSimplex os, Vector2 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return (float) os.GetDouble(v, scale, octaves, persistance, lacunarity, frequency, amplitude);
        }

        public static float GetFloat01(this OpenSimplex os, Vector2 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return (float) os.GetDouble01(v, scale, octaves, persistance, lacunarity, frequency, amplitude);
        }

        public static double GetDouble01(this OpenSimplex os, Vector2 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return os.GetDouble(v, scale, octaves, persistance, lacunarity, frequency, amplitude) * 0.5 + 0.5;
        }

        public static double GetDouble(this OpenSimplex os, Vector2 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            scale = Math.Max(scale, 0.00001);
            octaves = Math.Max(octaves, 1);

            double noise = 0;
            double x = v.x / scale * frequency;
            double y = v.y / scale * frequency;

            var simplex = os;
            for (var o = 0; o < octaves; o++)
            {
                noise += simplex.Evaluate(x, y) * amplitude;
                amplitude *= persistance;
                x *= lacunarity;
                y *= lacunarity;
            }

            return noise;
        }

        public static float GetFloat(this OpenSimplex os, Vector3 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return (float) os.GetDouble(v, scale, octaves, persistance, lacunarity, frequency, amplitude);
        }

        public static float GetFloat01(this OpenSimplex os, Vector3 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return (float) os.GetDouble01(v, scale, octaves, persistance, lacunarity, frequency, amplitude);
        }

        public static double GetDouble01(this OpenSimplex os, Vector3 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return os.GetDouble(v, scale, octaves, persistance, lacunarity, frequency, amplitude) * 0.5 + 0.5;
        }

        public static double GetDouble(this OpenSimplex os, Vector3 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            scale = Math.Max(scale, 0.00001);
            octaves = Math.Max(octaves, 1);

            double noise = 0;
            double x = v.x / scale * frequency;
            double y = v.y / scale * frequency;
            double z = v.z / scale * frequency;

            var simplex = os;
            for (var o = 0; o < octaves; o++)
            {
                noise += simplex.Evaluate(x, y, z) * amplitude;
                amplitude *= persistance;
                x *= lacunarity;
                y *= lacunarity;
                z *= lacunarity;
            }

            return noise;
        }

        public static float GetFloat(this OpenSimplex os, Vector4 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return (float) os.GetDouble(v, scale, octaves, persistance, lacunarity, frequency, amplitude);
        }

        public static float GetFloat01(this OpenSimplex os, Vector4 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return (float) os.GetDouble01(v, scale, octaves, persistance, lacunarity, frequency, amplitude);
        }

        public static double GetDouble01(this OpenSimplex os, Vector4 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            return os.GetDouble(v, scale, octaves, persistance, lacunarity, frequency, amplitude) * 0.5 + 0.5;
        }

        public static double GetDouble(this OpenSimplex os, Vector4 v, double scale = 1.0, int octaves = 1, double persistance = 1.0, double lacunarity = 1.0, double frequency = 1.0, double amplitude = 1.0)
        {
            scale = Math.Max(scale, 0.00001);
            octaves = Math.Max(octaves, 1);

            double noise = 0;
            double x = v.x / scale * frequency;
            double y = v.y / scale * frequency;
            double z = v.z / scale * frequency;
            double w = v.w / scale * frequency;

            var simplex = os;
            for (var o = 0; o < octaves; o++)
            {
                noise += simplex.Evaluate(x, y, z, w) * amplitude;
                amplitude *= persistance;

                x *= lacunarity;
                y *= lacunarity;
                z *= lacunarity;
                w *= lacunarity;
            }

            return noise;
        }

        #endregion
    }
}
