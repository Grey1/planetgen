﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System.Runtime.CompilerServices;
using UnityEngine;

namespace Grey.Utilities.Extensions
{
    public static class MathExtensions
    {
        public const float InversePI = 0.318309886183790f; //0.31830988618379067153776752674503;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double Clamp(double min, double max, double value)
        {
            if (value < min) return min;
            if (value > max) return max;
            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float RatioToMiddle(float min, float max, float value)
        {
            // find the [0:1] scaler between min & max
            var scaler = Mathf.InverseLerp(min, max, value);

            // double it, and take 1, this gives us a range [-1:1]
            scaler = scaler * 2f - 1f;

            // get the absolute value (-1.0 == 1.0)
            return Mathf.Abs(scaler); // min & max = 1.0, middle = 0.0
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float RatioFromMiddle(float min, float max, float value)
        {
            // min & max = 0.0, middle = 1.0
            return 1f - RatioToMiddle(min, max, value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Coserp(float start, float end, float value)
        {
            return Mathf.Lerp(start, end, 1.0f - Mathf.Cos(value * Mathf.PI * 0.5f));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int FastFloor(double x)
        {
            var xi = (int) x;
            return x < xi ? xi - 1 : xi;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 Vector3ToSphericalUV(Vector3 p)
        {
            var d = p.normalized;
            var u = Mathf.Atan2(-d.z, -d.x) * InversePI * 0.5f + 0.5f;
            var v = 1.0f - (0.5f - Mathf.Asin(d.y) * InversePI);
            return new Vector2(u, v);
        }

        /* shader version of Vector3ToSphericalUV
        #define InvPI 0.318309886183790
        inline float2 PolarCoords(float3 v3)
        {
            float3 d = normalize(v3);
            float u = atan2(-d.z, -d.x) * InvPI * 0.5 + 0.5;
            float v = 0.5 - asin(d.y) * InvPI;
            v = (v / 0.75);
            v = 1.0 - v;
            return float2(u, v);
        }
        */

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 SphericalUVtoVector3(float u, float v)
        {
            float x, y, z;
            SphericalUVtoVector3(u, v, out x, out y, out z);
            return new Vector3(x, y, z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SphericalUVtoVector3(float u, float v, out float x, out float y, out float z)
        {
            var fRdx = u * 2 * Mathf.PI;
            var fRdy = v * Mathf.PI;
            var fYSin = Mathf.Sin(fRdy + Mathf.PI);
            x = Mathf.Sin(fRdx) * fYSin;
            y = Mathf.Cos(fRdx) * fYSin;
            z = Mathf.Cos(fRdy);
        }

        public static Vector3 ImprovedCubeSphereVertexDistribution(Vector3 v)
        {
            // Distribution math 'borrowed' from http://catlikecoding.com/unity/tutorials/cube-sphere/
            var x2 = v.x * v.x;
            var y2 = v.y * v.y;
            var z2 = v.z * v.z;
            v.x = v.x * Mathf.Sqrt(1f - y2 / 2f - z2 / 2f + y2 * z2 / 3f);
            v.y = v.y * Mathf.Sqrt(1f - x2 / 2f - z2 / 2f + x2 * z2 / 3f);
            v.z = v.z * Mathf.Sqrt(1f - x2 / 2f - y2 / 2f + x2 * y2 / 3f);
            return v;
        }
    }
}
