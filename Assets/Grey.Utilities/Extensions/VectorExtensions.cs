﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using UnityEngine;

namespace Grey.Utilities.Extensions
{
    public static class VectorExtensions
    {
        public static Vector2 Clamp01(this Vector2 v)
        {
            return new Vector2(Mathf.Clamp01(v.x),
                               Mathf.Clamp01(v.y));
        }

        public static Vector3 Clamp01(this Vector3 v)
        {
            return new Vector3(Mathf.Clamp01(v.x),
                               Mathf.Clamp01(v.y),
                               Mathf.Clamp01(v.z));
        }

        public static Vector4 Clamp01(this Vector4 v)
        {
            return new Vector4(Mathf.Clamp01(v.x),
                               Mathf.Clamp01(v.y),
                               Mathf.Clamp01(v.z),
                               Mathf.Clamp01(v.w));
        }

        public static Vector2 Round(this Vector2 v)
        {
            return new Vector2(Mathf.Round(v.x),
                               Mathf.Round(v.y));
        }

        public static Vector3 Round(this Vector3 v)
        {
            return new Vector3(Mathf.Round(v.x),
                               Mathf.Round(v.y),
                               Mathf.Round(v.z));
        }

        public static Vector4 Round(this Vector4 v)
        {
            return new Vector4(Mathf.Round(v.x),
                               Mathf.Round(v.y),
                               Mathf.Round(v.z),
                               Mathf.Round(v.w));
        }

        public static Vector2 Bilinear(Vector2 a, Vector2 b, Vector2 c, Vector2 d, float u, float v)
        {
            var abu = Vector2.Lerp(a, b, u);
            var cdu = Vector2.Lerp(c, d, u);
            return Vector2.Lerp(abu, cdu, v);
        }

        public static Vector3 Bilinear(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float u, float v)
        {
            var abu = Vector3.Lerp(a, b, u);
            var cdu = Vector3.Lerp(c, d, u);
            return Vector3.Lerp(abu, cdu, v);
        }

        public static Vector4 Bilinear(Vector4 a, Vector4 b, Vector4 c, Vector4 d, float u, float v)
        {
            var abu = Vector4.Lerp(a, b, u);
            var cdu = Vector4.Lerp(c, d, u);
            return Vector4.Lerp(abu, cdu, v);
        }
    }
}