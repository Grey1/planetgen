// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Grey.Utilities.Random
{
    /// <summary>
    /// This class is for when we want instanced Random Number Generators (RNG)
    /// Instantiate a copy of this, and use all functionality in a similar manner to the static version
    /// </summary>
    public class RNG
    {
        private readonly MersenneTwister _rng;

        /// <summary>
        /// The seed for this Random Number Generator
        /// </summary>
        public uint Seed { get; private set; }

        /// <summary>
        /// Generates a Random Number Generator, using input seed
        /// </summary>
        public RNG(uint seed)
        {
            Seed = seed;
            _rng = new MersenneTwister((uint) seed);
        }

        /// <summary>
        /// Generates a Random Number Generator, using DateTime.UtcNow.GetHashCode() to define it's seed
        /// </summary>
        public RNG()
        {
            Seed = (uint) DateTime.UtcNow.Millisecond;
            _rng = new MersenneTwister(Seed);
        }

        /// <summary>
        /// Returns a random Float between 0.0f inclusive and 1.0f exclusive
        /// </summary>
        public float value
        {
            get { return (float) NextDouble(); }
        }

        /// <summary>
        /// Returns an integer between 0 inclusive and Int32.MaxValue exclusive
        /// </summary>
        public int Next()
        {
            return _rng.Next();
        }

        /// <summary>
        /// Returns a non-negative integer between 0 inclusive and Max exclusive
        /// </summary>
        public int Next(int max)
        {
            return _rng.Next(max);
        }

        /// <summary>
        /// Returns a non-negative integer between Min inclusive and Max exclusive
        /// </summary>
        public int Next(int min, int max)
        {
            return _rng.Next(min, max);
        }

        /// <summary>
        /// Returns a random Double between 0.0 inclusive and 1.0 exclusive
        /// </summary>
        public double NextDouble()
        {
            return _rng.NextDouble();
        }

        /// <summary>
        /// Returns a random Vector3, internal values ranging from 0.0f inclusive and 1.0f exclusive
        /// </summary>
        public Vector3 Vector3()
        {
            return new Vector3(value, value, value);
        }

        /// <summary>
        /// Return a randomly chosen Colour built using the HSV colour cylinder 
        /// </summary>
        /// <param name="minH">Minimum Hue from 0-1f, default 0</param>
        /// <param name="maxH">Maximum Hue from 0-1f, default 1</param>
        /// <param name="minS">Minimum Saturation from 0-1f, default 1</param>
        /// <param name="maxS">Maximum Saturation from 0-1f, default 1</param>
        /// <param name="minV">Minimum Value from 0-1f, default 1</param>
        /// <param name="maxV">Maximum Value from 0-1f, default 1</param>
        /// <param name="minA">Minimum Alpha from 0-1f, default 1</param>
        /// <param name="maxA">Maximum Alpha from 0-1f, default 1</param>
        public Color ColourHSV(float minH = 0f, float maxH = 1f, float minS = 1f, float maxS = 1f, float minV = 1f, float maxV = 1f, float minA = 1f, float maxA = 1f)
        {
            var h = Mathf.Clamp01(Range(minH, maxH));
            var s = Mathf.Clamp01(Range(minS, maxS));
            var v = Mathf.Clamp01(Range(minV, maxV));

            var c = Color.HSVToRGB(h, s, v);

            if (minA < 1f) c.a = Range(minA, maxA);

            return c;
        }

        /// <summary>
        /// Return a randomly chosen Color built using the HSV color cylinder 
        /// </summary>
        /// <param name="minH">Minimum Hue from 0-1f, default 0</param>
        /// <param name="maxH">Maximum Hue from 0-1f, default 1</param>
        /// <param name="minS">Minimum Saturation from 0-1f, default 1</param>
        /// <param name="maxS">Maximum Saturation from 0-1f, default 1</param>
        /// <param name="minV">Minimum Value from 0-1f, default 1</param>
        /// <param name="maxV">Maximum Value from 0-1f, default 1</param>
        /// <param name="minA">Minimum Alpha from 0-1f, default 1</param>
        /// <param name="maxA">Maximum Alpha from 0-1f, default 1</param>
        public Color ColorHSV(float minH = 0f, float maxH = 1f, float minS = 1f, float maxS = 1f, float minV = 1f, float maxV = 1f, float minA = 1f, float maxA = 1f)
        {
            return ColourHSV(minH, maxH, minS, maxS, minV, maxV, minA, maxA);
        }

        // TODO: generalize these common functions somehow?
        /// <summary>
        /// /// Return a random index for a HashSet
        /// </summary>
        public int GetRandomIndex<T>(HashSet<T> hashSet)
        {
            return Range(0, hashSet.Count);
        }

        /// <summary>
        /// Return a random index for a list
        /// </summary>
        public int GetRandomIndex<T>(List<T> list)
        {
            return Range(0, list.Count);
        }

        /// <summary>
        /// Return a random item from a list
        /// </summary>
        public T GetRandom<T>(List<T> list)
        {
            var idx = GetRandomIndex(list);
            return list[idx];
        }

        /// <summary>
        /// Return a specified length, randomly chosen subset of items from within a list
        /// </summary>
        public List<T> GetRandomSubset<T>(List<T> list, int count)
        {
            if (count >= list.Count)
                return list;

            const int maxTries = 100;
            var tries = 0;
            var selected = new HashSet<int>();

            while (selected.Count < count && tries < maxTries)
            {
                var nextPropIndex = GetRandomIndex(list);

                if (selected.Contains(nextPropIndex))
                {
                    tries++;
                    continue;
                }

                selected.Add(nextPropIndex);
                tries = 0;
            }

            return selected.Select(i => list[i]).ToList();
        }

        /// <summary>
        /// Returns a specified length, randomly chosen subset of items from within a list, trading memory for speed.
        ///  Resulting list retains original sorting, just with random elements 'missing'.
        /// </summary>
        public List<T> GetRandomSubset_HIGHMEM_MaintainsOrder<T>(List<T> list, int count)
        {
            if (count >= list.Count)
                return list;

            var copy = new List<T>(list.Count);

            copy.AddRange(list);
            while (copy.Count > count)
            {
                var randomIndex = GetRandomIndex(copy);
                copy.RemoveAt(randomIndex);
            }

            return copy;
        }

        /// <summary>
        /// Returns a specified length, randomly chosen subset of items from within a list, trading memory for speed.
        /// </summary>
        public HashSet<T> GetRandomSubset_HIGHMEM<T>(HashSet<T> set, int count)
        {
            if (count >= set.Count)
                return set;

            var copy = new HashSet<T>(set);
            var result = new HashSet<T>();

            while (result.Count < count)
            {
                var randomIndex = GetRandomIndex(copy);

                // slow! requires iterating over the elements to find
                // the appropriate index!
                var element = copy.ElementAt(randomIndex);
                result.Add(element);
                copy.Remove(element);
            }

            return result;
        }

        /// <summary>
        /// Returns a specified length, randomly chosen subset of items from within a list, trading memory for speed.
        /// </summary>
        public List<T> GetRandomSubset_HIGHMEM<T>(List<T> list, int count)
        {
            if (count >= list.Count)
                return list;

            var copy = new List<T>(list.Count);
            copy.AddRange(list);
            var result = new List<T>(count);

            while (result.Count < count)
            {
                var randomIndex = GetRandomIndex(copy);
                result.Add(copy[randomIndex]);
                copy.RemoveAt(randomIndex);
            }

            return result;
        }

        /// <summary>
        /// Return a random index for an array
        /// </summary>
        public int GetRandomIndex<T>(T[] array)
        {
            return Range(0, array.Length);
        }

        /// <summary>
        /// Return a random item from an array
        /// </summary>
        public T GetRandom<T>(T[] array)
        {
            var idx = GetRandomIndex(array);
            return array[idx];
        }

        /// <summary>
        /// Return a specified length, randomly chosen subset of items from within an array
        /// </summary>
        public T[] GetRandomSubset<T>(T[] array, int count)
        {
            if (count >= array.Length)
                return array;

            const int maxTries = 100;
            var tries = 0;
            var selected = new HashSet<int>();

            while (selected.Count < count && tries < maxTries)
            {
                tries++;

                var nextPropIndex = GetRandomIndex(array);

                if (selected.Contains(nextPropIndex))
                    continue;

                selected.Add(nextPropIndex);
            }

            return selected.Select(i => array[i]).ToArray();
        }

        /// <summary>
        /// Shuffles the contents of the list in place.
        /// </summary>
        public void Shuffle<T>(List<T> list)
        {
            for (var i = list.Count; i > 1; i--)
            {
                var j = Range(0, i);
                var tmp = list[j];
                list[j] = list[i - 1];
                list[i - 1] = tmp;
            }
        }

        /// <summary>
        /// Shuffles the contents of the array in place.
        /// </summary>
        public void Shuffle<T>(T[] array)
        {
            for (var i = array.Length; i > 1; i--)
            {
                var j = Range(0, i);
                var tmp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = tmp;
            }
        }

        /// <summary>
        /// Random Range from min (inclusive) to max (exclusive)
        /// </summary>
        public int Range(int min, int max)
        {
            return Next(min, max);
        }

        /// <summary>
        /// Random range from min (inclusive) to max (inclusive)
        /// </summary>
        public float Range(float min, float max)
        {
            return min + value * (max - min);
        }

        /// <summary>
        /// Returns a random point on the unit circle
        /// </summary>
        public Vector2 onUnitCircle
        {
            get
            {
                var randomAngle = Angle;
                return new Vector2(Mathf.Cos(randomAngle), Mathf.Sin(randomAngle));
            }
        }

        /// <summary>
        /// Returns a random point on the unit sphere
        /// </summary>
        public Vector3 onUnitSphere
        {
            get
            {
                var longitude = Angle;
                var latitude = Mathf.PI - Mathf.Acos(Range(-1f, 1f));
                var latSin = Mathf.Sin(latitude); //need to scale the x and z so that the radius of the circle cross section gets smaller at the poles
                return new Vector3(Mathf.Cos(longitude) * latSin, Mathf.Cos(latitude), Mathf.Sin(longitude) * latSin);
            }
        }

        /// <summary>
        /// Return a random point within the unit sphere
        /// </summary>
        public Vector3 insideUnitSphere
        {
            get { return onUnitSphere * Mathf.Pow(value, .333333333333333f); }
        }

        /// <summary>
        /// Returns a random point within the unit circle
        /// </summary>
        public Vector2 insideUnitCircle
        {
            get { return onUnitCircle * Mathf.Sqrt(value); }
        }

        /// <summary>
        /// Random angle in radians (0 to 2PI) inclusive.
        /// </summary>
        public float Angle
        {
            get { return Range(0f, 6.2831853071795864769f); }
        }

        /// <summary>
        /// Get a Vector3 on a circle projected onto the unit sphere surface, around the input point
        /// </summary>
        public Vector3 GetRandomLocationOnCircleCap(Vector3 point, float circleRadius, float sphereRadius)
        {
            var rotation = Quaternion.LookRotation(point);

            var newPoint = onUnitCircle * circleRadius;
            var rotated = rotation * newPoint;
            var offset = point + rotated;

            return offset.normalized * sphereRadius;
        }

        /// <summary>
        /// Get a Vector3 in a circle projected onto the unit sphere surface, around the input point
        /// </summary>
        public Vector3 GetRandomLocationInCircleCap(Vector3 point, float circleRadius, float sphereRadius)
        {
            var rotation = Quaternion.LookRotation(point);

            var newPoint = insideUnitCircle * circleRadius;
            var rotated = rotation * newPoint;
            var offset = point + rotated;

            return offset.normalized * sphereRadius;
        }

        /// <summary>
        /// Get a Vector3 in a ring projected onto the unit sphere surface, around the input point
        /// </summary>
        public Vector3 GetRandomLocationOnRingCap(Vector3 point, float sqrMinRadius, float sqrMaxRadius, float sphereRadius)
        {
            var rotation = Quaternion.LookRotation(point);

            var radius = Mathf.Sqrt(Range(sqrMinRadius, sqrMaxRadius)); //uniformly distributed along the ring (equal chance from min to max)
            var newPoint = onUnitCircle * radius;
            var rotated = rotation * newPoint;
            var offset = point + rotated;

            return offset.normalized * sphereRadius;
        }
    }
}
