// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System.Collections.Generic;
using UnityEngine;

namespace Grey.Utilities.Random
{
    /// <summary>
    /// This static class is for global access to a convenient Random Number Generator
    /// It uses a single internal RNG and all accesses go through that
    /// </summary>
    public static class RNGStatic
    {
        private static RNG _rng = new RNG();

        /// <summary>
        /// !STATIC! The seed for the globally accessible Random Number Generator
        /// </summary>
        public static uint Seed
        {
            get { return _rng.Seed; }
            set
            {
                if (_rng != null)
                {
                    // this simply stops the first-write of the seed value from giving a warning.
                    if (_seedSet)
                        Debug.LogWarning("Overwriting the static RNG seed after first usage.");
                    else
                        _seedSet = true;
                }

                _rng = new RNG(value);
            }
        }

        private static bool _seedSet = false;

        /// <summary>
        /// !STATIC! Returns a random Float between 0.0f inclusive and 1.0f exclusive
        /// </summary>
        public static float value { get { return _rng.value; } }

        /// <summary>
        /// !STATIC! Returns an integer between 0 inclusive and Int32.MaxValue exclusive
        /// </summary>
        public static int Next() { return _rng.Next(); }

        /// <summary>
        /// !STATIC! Returns a non-negative integer between 0 inclusive and Max exclusive
        /// </summary>
        public static int Next(int max) { return _rng.Next(max); }

        /// <summary>
        /// !STATIC! Returns a non-negative integer between Min inclusive and Max exclusive
        /// </summary>
        public static int Next(int min, int max) { return _rng.Next(min, max); }

        /// <summary>
        /// !STATIC! Returns a random Double between 0.0 inclusive and 1.0 exclusive
        /// </summary>
        public static double NextDouble() { return _rng.NextDouble(); }

        /// <summary>
        /// !STATIC! Returns a random Vector3, internal values ranging from 0.0f inclusive and 1.0f exclusive
        /// </summary>
        public static Vector3 Vector3() { return _rng.Vector3(); }

        /// <summary>
        /// !STATIC! Return a randomly chosen Colour built using the HSV colour cylinder 
        /// </summary>
        /// <param name="minH">Minimum Hue from 0-1f, default 0</param>
        /// <param name="maxH">Maximum Hue from 0-1f, default 1</param>
        /// <param name="minS">Minimum Saturation from 0-1f, default 1</param>
        /// <param name="maxS">Maximum Saturation from 0-1f, default 1</param>
        /// <param name="minV">Minimum Value from 0-1f, default 1</param>
        /// <param name="maxV">Maximum Value from 0-1f, default 1</param>
        /// <param name="minA">Minimum Alpha from 0-1f, default 1</param>
        /// <param name="maxA">Maximum Alpha from 0-1f, default 1</param>
        public static Color ColourHSV(float minH = 0f, float maxH = 1f, float minS = 1f, float maxS = 1f, float minV = 1f, float maxV = 1f, float minA = 1f, float maxA = 1f)
        {
            return _rng.ColourHSV(minH, maxH, minS, maxS, minV, maxV, minA, maxA);
        }

        /// <summary>
        /// !STATIC! Return a randomly chosen Color built using the HSV color cylinder 
        /// </summary>
        /// <param name="minH">Minimum Hue from 0-1f, default 0</param>
        /// <param name="maxH">Maximum Hue from 0-1f, default 1</param>
        /// <param name="minS">Minimum Saturation from 0-1f, default 1</param>
        /// <param name="maxS">Maximum Saturation from 0-1f, default 1</param>
        /// <param name="minV">Minimum Value from 0-1f, default 1</param>
        /// <param name="maxV">Maximum Value from 0-1f, default 1</param>
        /// <param name="minA">Minimum Alpha from 0-1f, default 1</param>
        /// <param name="maxA">Maximum Alpha from 0-1f, default 1</param>
        public static Color ColorHSV(float minH = 0f, float maxH = 1f, float minS = 1f, float maxS = 1f, float minV = 1f, float maxV = 1f, float minA = 1f, float maxA = 1f)
        {
            return _rng.ColorHSV(minH, maxH, minS, maxS, minV, maxV, minA, maxA);
        }

        /// <summary>
        /// !STATIC! Return a random index for a list
        /// </summary>
        public static int GetRandomIndex<T>(this List<T> list) { return _rng.GetRandomIndex(list); }

        /// <summary>
        /// !STATIC! Return a random item from a list
        /// </summary>
        public static T GetRandom<T>(this List<T> list) { return _rng.GetRandom(list); }

        /// <summary>
        /// !STATIC! Return a specified length, randomly chosen subset of items from within a list
        /// </summary>
        public static List<T> GetRandomSubset<T>(this List<T> list, int count) { return _rng.GetRandomSubset(list, count); }

        /// <summary>
        /// !STATIC! Return a random index for an array
        /// </summary>
        public static int GetRandomIndex<T>(this T[] array) { return _rng.GetRandomIndex(array); }

        /// <summary>
        /// !STATIC! Return a random item from an array
        /// </summary>
        public static T GetRandom<T>(this T[] array) { return _rng.GetRandom(array); }

        /// <summary>
        /// !STATIC! Return a specified length, randomly chosen subset of items from within an array
        /// </summary>
        public static T[] GetRandomSubset<T>(this T[] array, int count) { return _rng.GetRandomSubset(array, count); }

        /// <summary>
        /// !STATIC! Shuffles the contents of the list in place.
        /// </summary>
        public static void Shuffle<T>(this List<T> list) { _rng.Shuffle(list); }

        /// <summary>
        /// !STATIC! Shuffles the contents of the array in place.
        /// </summary>
        public static void Shuffle<T>(this T[] array) { _rng.Shuffle(array); }

        /// <summary>
        /// !STATIC! Random Range from min (inclusive) to max (exclusive)
        /// </summary>
        public static int Range(int min, int max) { return Next(min, max); }

        /// <summary>
        /// !STATIC! Random range from min (inclusive) to max (inclusive)
        /// </summary>
        public static float Range(float min, float max) { return min + value * (max - min); }

        /// <summary>
        /// !STATIC! Returns a random point on the unit circle
        /// </summary>
        public static Vector3 onUnitSphere { get { return _rng.onUnitSphere; } }

        /// <summary>
        /// !STATIC! Returns a random point on the unit sphere
        /// </summary>
        public static Vector2 onUnitCircle { get { return _rng.onUnitCircle; } }

        /// <summary>
        /// !STATIC! Return a random point within the unit sphere
        /// </summary>
        public static Vector3 insideUnitSphere { get { return _rng.insideUnitSphere; } }

        /// <summary>
        /// !STATIC! Returns a random point within the unit circle
        /// </summary>
        public static Vector2 insideUnitCircle { get { return _rng.insideUnitCircle; } }

        /// <summary>
        /// !STATIC! Random angle in radians (0 to 2PI) inclusive.
        /// </summary>
        public static float Angle { get { return _rng.Angle; } }

        /// <summary>
        /// !STATIC! Get a Vector3 on a circle projected onto the unit sphere surface, around the input point
        /// </summary>
        public static Vector3 GetRandomLocationOnCircleCap(Vector3 point, float circleRadius, float sphereRadius)
        {
            return _rng.GetRandomLocationOnCircleCap(point, circleRadius, sphereRadius);
        }

        /// <summary>
        /// !STATIC! Get a Vector3 in a ring projected onto the unit sphere surface, around the input point
        /// </summary>
        public static Vector3 GetRandomLocationOnRingCap(Vector3 point, float sqrMinRadius, float sqrMaxRadius, float sphereRadius)
        {
            return _rng.GetRandomLocationOnRingCap(point, sqrMinRadius, sqrMaxRadius, sphereRadius);
        }
    }
}