// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System;
using System.Collections;
using UnityEngine;

namespace Grey.Utilities
{
    public class Tools : MonoBehaviour
    {
        private static Tools _instance;

        private static Tools Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject("Grey.Utilities.Tools");
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<Tools>();
                }

                return _instance;
            }
        }

        public static void DoInvoke(Action action, float time)
        {
            DoCoroutine(WaitAction(action, time));
        }

        private static IEnumerator WaitAction(Action action, float time)
        {
            yield return new WaitForSeconds(time);
            if (action != null)
                action();
        }

        public static Coroutine DoCoroutine(IEnumerator routine)
        {
            return Instance.StartCoroutine(routine);
        }

        public static IEnumerator CountDown(int seconds, bool debug = false)
        {
            if (debug) Debug.LogWarningFormat("Starting countdown: {0} seconds remaining.", seconds);
            while (seconds > 0)
            {
                seconds--;
                yield return new WaitForSeconds(1f);
                if (debug) Debug.LogWarningFormat("Countdown: {0} seconds remaining.", seconds);
            }
        }
    }
}
