// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System;
using UnityEngine;

namespace Grey.Utilities
{
    public static class TemperatureUtils
    {
        public enum ReadoutType
        {
            Celcius = 0,
            Kelvin = 1,
            Fahrenheit = 2,
            Rankine = 3,
            Felcius = 4, // https://xkcd.com/1923/
        }

        public static ReadoutType Type = ReadoutType.Celcius;

        public static string ToReadoutString(float celcius, bool roundToInt = true, bool showSign = true)
        {
            return ToReadoutString(celcius, Type, roundToInt, showSign);
        }

        public static string ToReadoutString(float celcius, ReadoutType type, bool roundToInt = true, bool showSign = true)
        {
            switch (type)
            {
                case ReadoutType.Celcius: return celcius.Fmt(roundToInt, showSign);
                case ReadoutType.Kelvin: return celcius.ToKelvin().Fmt(roundToInt, showSign);
                case ReadoutType.Fahrenheit: return celcius.ToFahrenheit().Fmt(roundToInt, showSign);
                case ReadoutType.Rankine: return celcius.ToRankine().Fmt(roundToInt, showSign);
                case ReadoutType.Felcius: return celcius.ToFelcius().Fmt(roundToInt, showSign);
                default: throw new ArgumentOutOfRangeException();
            }
        }

        private static string Fmt(this float i, bool roundToInt, bool showSign)
        {
            var str = roundToInt ? Mathf.RoundToInt(i).ToString("N0") : i.ToString("N1");
            return showSign ? str + Sign : str;
        }

        private static string Sign
        {
            get
            {
                switch (Type)
                {
                    case ReadoutType.Celcius: return "°C";
                    case ReadoutType.Kelvin: return " K"; // no degrees symbol for Kelvin!
                    case ReadoutType.Fahrenheit: return "°F";
                    case ReadoutType.Rankine: return "°R";
                    case ReadoutType.Felcius: return "°Є";  // Closest I could get to the suggested symbol: https://en.wikipedia.org/wiki/Ukrainian_Ye
                    default: throw new ArgumentOutOfRangeException();
                }
            }
        }

        public static float ToKelvin(this float celcius)
        {
            return celcius + 273.15f;
        }

        public static float ToFahrenheit(this float celcius)
        {
            return Mathf.Round(celcius * 9f / 5f + 32f);
        }

        public static float ToRankine(this float celcius)
        {
            return celcius.ToFahrenheit() + 459.67f;
        }

        public static float ToFelcius(this float celcius)
        {
            return 7.0f * celcius / 5.0f + 16.0f;
        }
    }
}
