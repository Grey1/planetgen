﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System.Diagnostics;
using UnityEngine;

namespace Grey.Utilities
{
    public static class Time
    {
        private static readonly Stopwatch _stopwatch = new Stopwatch();
        private static readonly WaitForEndOfFrame _wait = new WaitForEndOfFrame();

        static Time()
        {
            _stopwatch.Start();
        }

        // extreme cpu usage on the main unity thread won't block this timer
        // very useful for getting *accurate* time measurements, and limiting
        // allowed cpu time to specific coroutines (for example)
        public static long UnblockedTime
        {
            get { return _stopwatch.ElapsedMilliseconds; }
        }

        public static void ResetUnblockedTime()
        {
            _stopwatch.Reset();
            _stopwatch.Start();
        }

        public static long FrameTime
        {
            get { return Mathf.FloorToInt(UnityEngine.Time.deltaTime * 1000); }
        }

        public static long FrameTimeUnscaled
        {
            get { return Mathf.FloorToInt(UnityEngine.Time.unscaledDeltaTime * 1000); }
        }

        public static WaitForEndOfFrame WaitForEndOfFrame
        {
            //get { return new WaitForEndOfFrame(); }
            get { return _wait; }
        }
    }
}
