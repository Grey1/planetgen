﻿using Grey.Utilities.Extensions;
using Grey.Utilities.Random;
using System;
using System.Collections;
using UnityEngine;
using Time = Grey.Utilities.Time;

namespace remove_this_later
{

    public class ObjSpawner : MonoBehaviour
    {
        public uint RNG_seed = 1337;
        private Transform _parent;
        public GameObject[] Prefabs;
        [Layer] public int ObjLayer;
        public int MaxAttemptCount = 100;
        public float GlobalObjectScaler = 0.1f;

        public IEnumerator SpawnObjects(int count, Vector3 planetPosition, float planetRadius, int planetPhysicsLayer, int allowedTimePerFrame, Action onComplete)
        {
            if (_parent == null)
            {
                var go = new GameObject("ObjParent");
                _parent = go.transform;
                _parent.parent = transform;
            }

            var rng = new RNG(RNG_seed);
            for (int i = 0; i < count; i++)
            {
                var start = rng.onUnitSphere.normalized * planetRadius * 1.5f;

                var ray = new Ray(start, planetPosition - start);

                var newObj = Instantiate(Prefabs.GetRandom());
                var col = newObj.GetComponent<MeshCollider>();
                var rad = col.bounds.size.magnitude * GlobalObjectScaler;

                var tries = 0;
                bool hit;
                RaycastHit hitInfo;

                do
                {
                    if (Time.UnblockedTime >= allowedTimePerFrame)
                    {
                        yield return Time.WaitForEndOfFrame;
                        Time.ResetUnblockedTime();
                    }

                    tries++;
                    hit = Physics.Raycast(ray, out hitInfo, float.PositiveInfinity, planetPhysicsLayer);

                    if (hit)
                    {
                        if (Physics.CheckSphere(hitInfo.point, rad, ObjLayer))
                            continue;
                        break;
                    }

                    if (tries > MaxAttemptCount)
                    {
                        Debug.LogFormat("Attempted to find a usable object spawn location over {0:N0} times, aborting", MaxAttemptCount);
                        DestroyImmediate(newObj);
                        _parent.name = "ObjParent: " + _parent.childCount;
                        onComplete?.Invoke();
                        yield break;
                    }
                } while (!hit);

                newObj.layer = ObjLayer;
                newObj.transform.parent = _parent;
                newObj.transform.localScale = Vector3.one * GlobalObjectScaler;
                newObj.transform.position = hitInfo.point;
                newObj.transform.up = (hitInfo.point - planetPosition).normalized;
            }

            _parent.name = "ObjParent: " + _parent.childCount;
            onComplete?.Invoke();
        }
    }
}
