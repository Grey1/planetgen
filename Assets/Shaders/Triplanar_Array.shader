﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


// Additional Note:
// I make no pretense that this is original,
// or even good! But aim to make it at least
// relatively easily understood.

Shader "Custom/Triplanar_Array"
{
	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "AutoLight.cginc"

		#define epsilon 0.0001

		int _TextureCount = 0;

		float _planetRadius;
		float _heightOffsetMax;

		float _endHeights[32];
		float _blendPercents[32];
		float _textureScales[32];

		float4 _emissionColours[32];
		float _emissionStrengths[32];

		float _Smoothness;
		float _Metallicness;

		float _useNMap;

		UNITY_DECLARE_TEX2DARRAY(_MainTex);
		UNITY_DECLARE_TEX2DARRAY(_BumpTex);
		UNITY_DECLARE_TEX2DARRAY(_EmissionTex);
		UNITY_DECLARE_TEX2DARRAY(_MetalnessTex);

		// Inverse lerp: calculate the scaler [0..1] which would
		// result in the output value provided.
		inline float InverseLerp(float minVal, float maxVal, float val)
		{
			float top = (val - minVal);

			// this mess is to avoid any potential divide-by-zero (without branching)
			float bot = (maxVal - minVal);

			// get the sign of 'bot'
			float botsign = sign(bot);

			// ensure that the absolute value of 'bot' is above zero
			bot = max(epsilon, abs(bot));

			// reassign the sign to our 'bot' value (so negative values stay negative)
			bot = bot * botsign;

			// actually do the dang inverse lerp calculation
			return saturate(top / bot);
		}

		inline float calculateIndexes(float3 objectPosition, out float prev, out float idx, out float next)
		{
			// calculate the distance from object 0,0 (ie: fragment radius)
			float rad = length(objectPosition);

			// get how much difference there is between the planet radius, and the fragment radius
			float heightOffset = rad - _planetRadius;

			// calculate a scaler for our height offset
			float heightPercentage = InverseLerp(-_heightOffsetMax, _heightOffsetMax, heightOffset);

			// find the index of the texture we should use, based upon the height of the fragment
			for (int i = 0; i < _TextureCount; i++)
			{
				float cur = heightPercentage;
				float end = _endHeights[i];

				// if the current height is lower than the end height,
				// then we will get a negative number.
				// sign returns either -1 or 1, depending on sign
				// we then use max to raise that -1 to a 0
				// meaning our index will be += 0

				// if the current height is exactly equal, we take 0.0001 
				// from the sign output, which treats it as 'lower'
				// aka: idx += 0

				// if the current height is above the end height,
				// sign will output 1, which means our index goes up
				int j = max(0, sign((cur - end) - 0.0001));
				idx += j;

				// and no branching.
			}

			prev = max(0, idx - 1);
			next = min(_TextureCount - 1, idx + 1);

			return heightPercentage;
		}

		inline float3 calculateBlendWeights(float3 n)
		{
			float3 b = abs(n);
			return b / (b.x + b.y + b.z);
		}

		inline void calculateBlendValues(float prev, float idx, float heightPercentage, out float baseBlend, out float highBlend)
		{
			// either this will give us the range of our current sample,
			// or it will return 0 (in the case that 'prev' is clamped to
			// the same index as our current end height.
			float startHeight = _endHeights[idx] - _endHeights[prev];

			// find out how far we are between our start and end
			float current = InverseLerp(startHeight, _endHeights[idx], heightPercentage);

			float blendStart = 1.0 - _blendPercents[idx];

			highBlend = InverseLerp(blendStart, 1.0, current);
			baseBlend = 1 - highBlend;
		}

		inline half3 TriSampleAlbedo(float3 xUV, float3 yUV, float3 zUV, float idx, float3 blendWeights, out float metallicness)
		{
			float s = _textureScales[idx];

			float4 xDiff = UNITY_SAMPLE_TEX2DARRAY(_MainTex, float3(xUV.x / s, xUV.y / s, idx));
			float4 yDiff = UNITY_SAMPLE_TEX2DARRAY(_MainTex, float3(yUV.x / s, yUV.y / s, idx));
			float4 zDiff = UNITY_SAMPLE_TEX2DARRAY(_MainTex, float3(zUV.x / s, zUV.y / s, idx));

			float4 final =  xDiff * blendWeights.x + yDiff * blendWeights.y + zDiff * blendWeights.z;

			metallicness = final.a;

			return final.rgb;
		}

		inline float3 rnmBlendUnpacked(float3 n1, float3 n2)
		{
			n1 += float3(0, 0, 1);
			n2 *= float3(-1, -1, 1);
			return n1*dot(n1, n2) / n1.z - n2;
		}

		inline float3 TriSampleNormal(float3 xUV, float3 yUV, float3 zUV, float idx, float3 worldNormal, float3 blendWeights, out float smoothness)
		{
			float s = _textureScales[idx];
			
			float4 sx = UNITY_SAMPLE_TEX2DARRAY(_BumpTex, float3(xUV.x / s, xUV.y / s, idx));
			float4 sy = UNITY_SAMPLE_TEX2DARRAY(_BumpTex, float3(yUV.x / s, yUV.y / s, idx));
			float4 sz = UNITY_SAMPLE_TEX2DARRAY(_BumpTex, float3(zUV.x / s, zUV.y / s, idx));

			// extract the smoothness values stored in the alpha channel
			smoothness =
				sx.a* blendWeights.x +
				sy.a* blendWeights.y +
				sz.a* blendWeights.z;

			float3 tnx = UnpackNormal(sx);
			float3 tny = UnpackNormal(sy);
			float3 tnz = UnpackNormal(sz);

			float3 absVertNormal = abs(worldNormal);

			tnx = rnmBlendUnpacked(float3(worldNormal.zy, absVertNormal.x), tnx);
			tny = rnmBlendUnpacked(float3(worldNormal.xz, absVertNormal.y), tny);
			tnz = rnmBlendUnpacked(float3(worldNormal.xy, absVertNormal.z), tnz);

			float3 axisSign = sign(worldNormal);

			return normalize(
				tnx * blendWeights.x +
				tny * blendWeights.y +
				tnz * blendWeights.z +
				worldNormal);
		}

		inline float3 TriSampleEmission(float3 xUV, float3 yUV, float3 zUV, float idx, float3 blendWeights)
		{
			float s = _textureScales[idx];

			float3 xDiff = UNITY_SAMPLE_TEX2DARRAY(_EmissionTex, float3(xUV.x / s, xUV.y / s, idx));
			float3 yDiff = UNITY_SAMPLE_TEX2DARRAY(_EmissionTex, float3(yUV.x / s, yUV.y / s, idx));
			float3 zDiff = UNITY_SAMPLE_TEX2DARRAY(_EmissionTex, float3(zUV.x / s, zUV.y / s, idx));
			return xDiff * blendWeights.x + yDiff * blendWeights.y + zDiff * blendWeights.z;
		}

		struct VertexData
		{
			float4 position : POSITION;
			float3 normal : NORMAL;
			float3 objNormal : TEXCOORD0;
		};

		struct Interpolators
		{
			float4 position : SV_POSITION;
			float3 worldSpaceNormal : NORMAL;
			float3 objectPosition : TEXCOORD0;
			float3 worldPosition : TEXCOORD1;

#if defined(VERTEXLIGHT_ON)
			float3 vertexLightColour : TEXCOORD3;
#endif

#if defined(SHADOWS_SCREEN)
			float4 shadowCoordinates : TEXCOORD4;
#endif
		};

		inline void ComputeVertextLightColour(VertexData v, inout Interpolators i)
		{
#if defined(VERTEXLIGHT_ON)
			i.vertexLightColour = Shade4PointLights(
				unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
				unity_LightColor[0].rgb, unity_LightColor[1].rgb,
				unity_LightColor[2].rgb, unity_LightColor[3].rgb,
				unity_4LightAtten0, i.worldPosition, i.worldSpaceNormal
			);
#endif
		}

		inline UnityLight CreateLight(float3 worldSpaceNormal, float3 worldPosition, float2 shadowUVs)
		{
			UnityLight light;

#if defined(POINT) || defined(POINT_COOKIE) || defined(SPOT)
			light.dir = normalize(_WorldSpaceLightPos0 - worldPosition);
#else
			light.dir = _WorldSpaceLightPos0.xyz;
#endif

#if defined(SHADOWS_SCREEN)
			float attenuation = tex2D(_ShadowMapTexture, shadowUVs);
#else
			UNITY_LIGHT_ATTENUATION(attenuation, 0, worldPosition);
#endif

			light.color = _LightColor0.rgb * attenuation;
			light.ndotl = DotClamped(worldSpaceNormal, light.dir);
			return light;
		}

		inline UnityIndirect CreateIndirectLight(Interpolators i)
		{
			UnityIndirect indirectLight;
			indirectLight.diffuse = 0;
			indirectLight.specular = 0;

#if defined(VERTEXLIGHT_ON)
			indirectLight.diffuse = i.vertexLightColour;
#endif

#if defined(FORWARD_BASE_PASS)
			indirectLight.diffuse += max(0, ShadeSH9(float4(i.worldSpaceNormal, 1)));
#endif

			return indirectLight;
		}

		inline Interpolators CommonVert(VertexData v)
		{
			Interpolators i;

			i.position = UnityObjectToClipPos(v.position);
			i.objectPosition = v.position;
			i.worldPosition = mul(unity_ObjectToWorld, v.position);
			i.worldSpaceNormal = UnityObjectToWorldNormal(v.normal);

#if defined(SHADOWS_SCREEN)
			i.shadowCoordinates = ComputeScreenPos(i.position);
#endif

			ComputeVertextLightColour(v, i);

			return i;
		}

		inline float4 CommonFrag(Interpolators i)
		{
			// this is our fallback - ensures less weirdness when the shader
			// data has not been set up correctly.
			if (_TextureCount < 1)
			{
				return 1;
			}



			// fix normal distortion through per-frag interpolation
			float3 wsn = normalize(i.worldSpaceNormal);
			float3 op = i.objectPosition;
			float3 wp = i.worldPosition;

			float2 x = op.zy;
			float2 y = op.xz;
			float2 z = op.xy;

			float3 xUV = float3(x.x, x.y, 0);
			float3 yUV = float3(y.x, y.y, 0);
			float3 zUV = float3(z.x, z.y, 0);

			// set up for our texture blending
			float prev, idx, next;
			float heightPercentage = calculateIndexes(op, prev, idx, next);
			float3 blendWeights = calculateBlendWeights(wsn);
			float baseBlend, highBlend;
			calculateBlendValues(prev, idx, heightPercentage, baseBlend, highBlend);

			// we blend the 'high' sample *backwards* into our current base.
			// blend the albedo and metallicness maps
			float3 albedo, metallicness;
			{
				float mbase, mhigh;
				float3 base = TriSampleAlbedo(xUV, yUV, zUV, idx, blendWeights, mbase);
				float3 high = TriSampleAlbedo(xUV, yUV, zUV, next, blendWeights, mhigh);
				albedo = (base * baseBlend) + (high * highBlend);
				metallicness = (mbase * baseBlend) + (mhigh * highBlend);
			}

			// blend normal & smoothness maps
			float3 smoothness, worldNormal;
			{
				float smbase, smhigh;
				float3 base = TriSampleNormal(xUV, yUV, zUV, idx, wsn, blendWeights, smbase);
				float3 high = TriSampleNormal(xUV, yUV, zUV, next, wsn, blendWeights, smhigh);
				float3 final = (base * baseBlend) + (high * highBlend);
				worldNormal = normalize(final);
				smoothness = (smbase * baseBlend) + (smhigh * highBlend);
			}

			// blend emission textures & colours
			float3 emission;
			{
				half3 baseColour = TriSampleEmission(xUV, yUV, zUV, idx, blendWeights);
				float alpha = Luminance(baseColour);
				float strength = max(0, _emissionStrengths[idx]);
				emission = baseColour * alpha * strength * baseBlend;
			}

			float3 viewDir = normalize(_WorldSpaceCameraPos - wp);

			float3 specularTint;
			float oneMinusReflectivity;

			albedo = DiffuseAndSpecularFromMetallic(
				albedo,
				metallicness,
				specularTint,
				oneMinusReflectivity);

#if defined(SHADOWS_SCREEN)
			float2 shadowUVs = i.shadowCoordinates.xy / i.shadowCoordinates.w;
#else
			float2 shadowUVs = 0;
#endif
			float4 output = UNITY_BRDF_PBS(
				albedo, specularTint,
				oneMinusReflectivity, smoothness,
				worldNormal, viewDir,
				CreateLight(worldNormal, wp, shadowUVs),
				CreateIndirectLight(i));

			return output + float4(emission, 1);
		}

		ENDCG

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM

			// minimum target 3.5 (DX10, OpenGLES3) for texture arrays
			#pragma target 3.5
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ VERTEXLIGHT_ON

			#define FORWARD_BASE_PASS

			Interpolators vert(VertexData v)
			{
				return CommonVert(v);
			}

			float4 frag(Interpolators i) : SV_TARGET
			{
				return CommonFrag(i);
			}
			ENDCG
		}
			
		Pass
		{
			Tags{ "LightMode" = "ForwardAdd" }

			Blend One One
			ZWrite Off

			CGPROGRAM

			// minimum target 3.5 (DX10, OpenGLES3) for texture arrays
			#pragma target 3.5
			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile_fwdadd_fullshadows

			Interpolators vert(VertexData v)
			{
				return CommonVert(v);
			}

			float4 frag(Interpolators i) : SV_TARGET
			{
				return CommonFrag(i);
			}
			ENDCG
		}

		Pass
		{
			Tags{ "LightMode" = "ShadowCaster" }

			CGPROGRAM

			#pragma target 3.5
			#pragma vertex vert
			#pragma fragment frag

			#pragma multi_compile_shadowcaster

#if defined(SHADOWS_CUBE)
			struct Data
			{
				float4 position : SV_POSITION;
				float3 lightVec : TEXCOORD0;
			};

			Data vert(VertexData v)
			{
				Data d;
				d.position = UnityObjectToClipPos(v.position);
				d.lightVec = mul(unity_ObjectToWorld, v.position).xyz - _LightPositionRange.xyz;
				return d;
			}

			float4 frag(Data d) : SV_TARGET
			{
				float depth = length(d.lightVec) + unity_LightShadowBias.x;
				depth *= _LightPositionRange.w;
				return UnityEncodeCubeShadowDepth(depth);
			}
#else
			float4 vert(VertexData v) : SV_POSITION
			{
				float4 pos = UnityClipSpaceShadowCasterPos(v.position, v.normal);
				return UnityApplyLinearShadowBias(pos);
			}

			float4 frag() : SV_TARGET
			{
				return 0;
			}
#endif

			ENDCG
		}
	}
}
