﻿Shader "Custom/DEBUG_Normals"
{
	Properties
	{
		[Toggle] _viewType("UseCalculatedNormals",int) = 1
	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert
		#include "UnityCG.cginc"

		int _viewType;

		struct Input
		{
			float3 calculatedNormal;
			float3 suppliedNormal;
			INTERNAL_DATA
		};

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.calculatedNormal = normalize(v.vertex);
			o.suppliedNormal = v.normal;
		}

		void surf(Input i, inout SurfaceOutput o)
		{
			// clamp between 0 & 1
			float c = clamp(0, 1, _viewType);
			// take correct colour output
			float3 colour = lerp(i.suppliedNormal, i.calculatedNormal, c);
			// apply colour
			o.Albedo = float4(colour, 0);

		}
		ENDCG
	}
}
