﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System;
using System.Collections;
using UnityEngine;
using Time = Grey.Utilities.Time;

public class CubeSphere
{
    public int AllowedTimePerFrame;

    private readonly Func<Vector3, float, float, float> _noiseGenerator;
    //private readonly CubeSphereTile[] _tiles = new CubeSphereTile[6];
    private readonly Material _planetMaterial;
    private readonly Transform _transform;
    private readonly float _heightScaler;
    private readonly float _radius;
    private readonly int _physicsLayer;

    private readonly int _maxLod;

    private CubeSphereTreeXY _front, _back;
    private CubeSphereTreeYZ _left, _right;
    private CubeSphereTreeXZ _top, _bottom;

    public CubeSphere(float planetRadius, Transform parent, int physicsLayer, Material planetMaterial, float heightmapScaler, Func<Vector3, float, float, float> noiseGenerator)
    {
        _radius = planetRadius;
        _transform = parent;
        _noiseGenerator = noiseGenerator;
        _planetMaterial = planetMaterial;
        _heightScaler = heightmapScaler;
        _physicsLayer = physicsLayer;

        // Essentially this calculates the LOD level required to make
        //  one 'chunk' equals 1 metre of space at ground level.
        // With a vert count of 100, this calculation will
        // guarantee a 1cm-per-vert ratio.
        _maxLod = Mathf.FloorToInt(Mathf.Log(_radius * 2f, 2f));
    }

    public IEnumerator InitialFaceGeneration()
    {
        for (int i = 0; i < 6; i++)
        {
            if (Time.UnblockedTime >= AllowedTimePerFrame)
            {
                yield return Time.WaitForEndOfFrame;
                Time.ResetUnblockedTime();
            }

            var face = (CubeSphereTile.Face) i;
            switch (face)
            {
                case CubeSphereTile.Face.Front:
                {
                    _front = new CubeSphereTreeXY(Vector3.forward * _radius, _radius, 0);
                    InitQTree(ref _front, face);
                    break;
                }
                case CubeSphereTile.Face.Back:
                {
                    _back = new CubeSphereTreeXY(Vector3.back * _radius, _radius, 0);
                    InitQTree(ref _back, face);
                    break;
                }
                case CubeSphereTile.Face.Left:
                {
                    _left = new CubeSphereTreeYZ(Vector3.left * _radius, _radius, 0);
                    InitQTree(ref _left, face);
                    break;
                }
                case CubeSphereTile.Face.Right:
                {
                    _right = new CubeSphereTreeYZ(Vector3.right * _radius, _radius, 0);
                    InitQTree(ref _right, face);
                    break;
                }
                case CubeSphereTile.Face.Top:
                {
                    _top = new CubeSphereTreeXZ(Vector3.up * _radius, _radius, 0);
                    InitQTree(ref _top, face);
                    break;
                }
                case CubeSphereTile.Face.Bottom:
                {
                    _bottom = new CubeSphereTreeXZ(Vector3.down * _radius, _radius, 0);
                    InitQTree(ref _bottom, face);
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // _tiles[i] = MakeFace(, 0);
            // uses unity functions, no threading here:
            //_tiles[i].SetMesh(_tiles[i].MakeMesh(), _planetMaterial);
        }
    }

    private void InitQTree<T>(ref T tree, CubeSphereTile.Face face) where T: CubeSphereTree
    {
        var item = MakeFace(face, 0);
        tree.Root.InsertItem(item);
        item.SetMesh(item.MakeMesh(), _planetMaterial);
    }

    private CubeSphereTile MakeFace(CubeSphereTile.Face face, int depth)
    {
        var tile = new CubeSphereTile(face, _transform, _physicsLayer, depth, Vector2.zero, Vector2.one);
        tile.GenerateData(_noiseGenerator, _radius, _heightScaler);
        return tile;
    }

    public IEnumerator RefreshFace(Action onComplete)
    {
        // eventually this nasty mess will be redundant. leaving as-is for now.
        for (int i = 0; i < 6; i++)
        {
            if (Time.UnblockedTime >= AllowedTimePerFrame)
            {
                yield return Time.WaitForEndOfFrame;
                Time.ResetUnblockedTime();
            }
            CubeSphereTile tile;
            switch (i)
            {
                case 0:
                    tile = _top.Root.Item;
                    break;
                case 1:
                    tile = _bottom.Root.Item;
                    break;
                case 2:
                    tile = _left.Root.Item;
                    break;
                case 3:
                    tile = _right.Root.Item;
                    break;
                case 4:
                    tile = _front.Root.Item;
                    break;
                case 5:
                    tile = _back.Root.Item;
                    break;
                default:
                    Debug.Log("wtf bbq");
                    yield break;
            }

            tile.GenerateData(_noiseGenerator, _radius, _heightScaler);
            var mesh = tile.MakeMesh();
            tile.SetMesh(mesh, _planetMaterial);
        }

        onComplete?.Invoke();
    }

    /*
    // not used for now - needs more work (LOD system)
    public void LODUpdate(Transform viewer)
    {
        var p = viewer.position;
        var closest = float.MaxValue;
        CubeSphereTile best = null;
        foreach (var tile in _tiles)
        {
            var dist = Vector3.Distance(p, tile.Centre);
            if (dist < closest)
            {
                closest = dist;
                best = tile;
            }
        }

        best?.ShowOrBuildBest(p, _noiseGenerator, _radius, _heightScaler, AllowedTimePerFrame);
    }
    */
}
