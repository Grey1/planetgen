﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using UnityEngine;

public class CubeSphereTreeXY : CubeSphereTree
{
    public override Node Root => _root;
    private readonly NodeXY _root;

    public CubeSphereTreeXY(Vector3 position, float size, int depth)
    {
        _root = new NodeXY(position, size, depth);
        NodePool<NodeXY>.FillPool();
    }

    protected class NodeXY : Node
    {
        public NodeXY()
        {
        }

        public NodeXY(Vector3 position, float size, int depth) : base(position, size, depth)
        {
        }

        protected override int CalculateOffset(Vector3 location, Vector3 target)
        {
            var offset = 0;
            offset |= (target.y > location.y) ? 0 : 2;
            offset |= (target.x > location.x) ? 1 : 0;
            return offset;
        }

        protected override void CreateChildren()
        {
            var cDepth = Depth - 1;
            Children[0] = NodePool<NodeXY>.GetNode(new Vector3(-QuarterSize, +QuarterSize, 0), HalfSize, cDepth);
            Children[1] = NodePool<NodeXY>.GetNode(new Vector3(+QuarterSize, +QuarterSize, 0), HalfSize, cDepth);
            Children[2] = NodePool<NodeXY>.GetNode(new Vector3(-QuarterSize, -QuarterSize, 0), HalfSize, cDepth);
            Children[3] = NodePool<NodeXY>.GetNode(new Vector3(+QuarterSize, -QuarterSize, 0), HalfSize, cDepth);
        }

        protected override void ReturnChildNode(int index)
        {
            NodePool<NodeXY>.ReturnNode((NodeXY) Children[index]);
        }
    }
}
