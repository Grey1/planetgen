﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(NewPlanet))]
public class NewPlanetEditor : Editor
{
    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Generate In Editor"))
        {
            var np =target as NewPlanet;
            if(np != null)
                np.GENERATE_FROM_EDITOR();
        }

        base.OnInspectorGUI();
    }
}
