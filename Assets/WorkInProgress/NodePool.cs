﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using System.Collections.Generic;
using UnityEngine;

public static class NodePool<T> where T : CubeSphereTree.Node, new()
{
    private const int PoolCount = 128;
    private static readonly Stack<T> UnusedNodes = new Stack<T>(PoolCount * 2);

    public static void FillPool()
    {
        while (UnusedNodes.Count < PoolCount)
        {
            UnusedNodes.Push(default(T));
        }
    }

    public static T GetNode(Vector3 position, float size, int depth)
    {
        T node;
        if (UnusedNodes.Count < 1)
            node = new T();
        else
            node = UnusedNodes.Pop() ?? new T();

        node.Initialize(position, size, depth);
        return node;
    }

    public static void ReturnNode(T node)
    {
        if (node != null)
            UnusedNodes.Push(node);
    }
}
