﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using UnityEngine;

public abstract class CubeSphereTree
{
    public abstract Node Root { get; }

    public abstract class Node
    {
        protected float QuarterSize;
        protected float HalfSize;
        protected float Size;

        public CubeSphereTile Item;
        protected Node[] Children;

        public int Depth { get; protected set; }
        public Bounds Bounds { get; protected set; }
        public Vector3 Position { get; protected set; }

        protected Node()
        {
        }

        protected Node(Vector3 position, float size, int depth)
        {
            Initialize(position, size, depth);
        }

        public void Initialize(Vector3 position, float size, int depth)
        {
            Position = position;
            Depth = depth;

            Size = size;
            HalfSize = size * 0.5f;
            QuarterSize = size * 0.25f;

            Bounds = new Bounds(position, Vector3.one * size);
        }

        /// <summary>
        /// Given the current node location, calculate which child is closest to the target point
        /// </summary>
        /// <param name="location">the location of the current node</param>
        /// <param name="target">the point we are attempting to find</param>
        /// <returns>the index of the child which is closest to the target point</returns>
        protected abstract int CalculateOffset(Vector3 location, Vector3 target);

        public void InsertItem(CubeSphereTile item)
        {
            Item = item;
        }

        public void RemoveItem()
        {
            Item = null;
        }

        protected abstract void CreateChildren();

        private void RemoveChildren()
        {
            for (var i = 0; i < 4; i++)
            {
                if (Children[i] != null)
                {
                    Children[i].RemoveChildren();
                    Children[i].RemoveItem();
                    ReturnChildNode(i);
                }
            }
        }

        protected Node GetChildNode(Vector3 position)
        {
            var idx = CalculateOffset(Position, position);
            return Children[idx];
        }

        protected abstract void ReturnChildNode(int index);
    }
}

public static class CubeSphereTreeExtensions
{
    public static void EnableNodes(this CubeSphereTree tree, Vector3 position)
    {
        //todo: this whole thing :P

        // this function should work it's way through the tree and for each node, calculate if it is within the 
        // required distance of the camera to either render or split.

        // if the node is close enough to split, it should NOT be marked to render.
        //  and it's children should each be tested in turn.

        // if the tested node *depth* is 0 then this node is not allowed to split, and is thus marked renderable.
        // (the root node is always the highest depth value, dropping by -1 for each level of children)

        // if the node is NOT close enough to split, stop the traversal and mark node as renderable.

        // if a node is marked as renderable, test if the node has a tile generated.
        // if the tile is generated, render it.
        // if the tile is NOT generated, mark it for generation >> todo: multi-thread the generation step

        // if the node is marked renderable, recursively destroy all child nodes >> todo: hysteresis so we're not creating and destroying the same nodes too often.

    }
}