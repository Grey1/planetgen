﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using UnityEngine;

public class OrbitCamera : MonoBehaviour {

    private enum Mode
    {
        Orbit,
        Freefly,
    }

    private Mode _mode = Mode.Orbit;

    private Transform _orbitTarget;
    private int _targetCollisionLayer;
    private float _orbitRadius;

    private RaycastHit _hitInfo;


    public void Setup(Transform target, int physicsLayer, float radius)
    {
        _orbitTarget = target;
        _orbitRadius = radius;
        _targetCollisionLayer = physicsLayer;
    }

    private void LateUpdate()
    {
        if (_orbitTarget == null)
            return;

        ZoomUpdate();
    }

    #region Basic Zoom Handling

    [Header("Zoom")]
    private float _desiredZoom = 1f;
    [SerializeField] private float _minZoom = 1f;
    [SerializeField] private float _maxZoom = 380f;
    [SerializeField] private float _zoomSpeed = 10f;
    [SerializeField] private AnimationCurve _zoomCurve;
    
    private float _currentMinZoom
    {
        get
        {
            var z = _orbitRadius + _minZoom;
            var ray = new Ray(transform.position, _orbitTarget.position - transform.position);
            if (Physics.Raycast(ray, out _hitInfo, float.PositiveInfinity, _targetCollisionLayer))
                z = Vector3.Distance(_orbitTarget.position, _hitInfo.point) + _minZoom;
            return z;
        }
    }

    private float _currentMaxZoom
    {
        get
        {
            var z = _orbitRadius + _maxZoom;
            return z;
        }
    }

    private float _currentZoom
    {
        get
        {
            var dist = Vector3.Distance(_orbitTarget.position, transform.position);
            var z = Mathf.InverseLerp(_currentMinZoom, _currentMaxZoom, dist);
            return z;
        }
    }

    private void ZoomUpdate()
    {
        if (_mode == Mode.Orbit)
        {
            var msw = Input.GetAxis("Mouse ScrollWheel");
            if (Mathf.Abs(msw) > 0.0001f)
            {
                _desiredZoom = Mathf.Clamp01(_desiredZoom - (msw / 5f));
            }

            if (Mathf.Abs(_desiredZoom - _currentZoom) > 0.01f)
            {
                var dir = (transform.position - _orbitTarget.position).normalized;
                var rad = Mathf.Lerp(_currentMinZoom, _currentMaxZoom, _desiredZoom);
                var final = _orbitTarget.position + (dir * rad);
                transform.position = Vector3.MoveTowards(transform.position, final, Time.deltaTime * _zoomSpeed);

                var curve = _zoomCurve.Evaluate(_currentZoom);

                // "land" on the planet
                transform.up = Vector3.Lerp(transform.up, dir, 1f - curve);
                if (Mathf.Abs(curve) <= 0.02f)
                {
                    _mode = Mode.Freefly;
                }
            }
        }
    }

    #endregion
}
