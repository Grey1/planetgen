﻿// Grey Utilities
// Copyright(c) 2017 Geoffrey Clark <grey@bagofbacon.com>
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

using Grey.Utilities.Extensions;
using System;
using UnityEngine;

public class CubeSphereTile
{
    // arbitrary!
    private const int Size = DebuggingMess.VertCount;

    public enum Face
    {
        Front = 0,
        Back = 1,
        Left = 2,
        Right = 3,
        Top = 4,
        Bottom = 5,
    }

    private readonly Face _face;

    public readonly CubeSphereTile[] Children = new CubeSphereTile[4];

    private readonly GameObject _gameObject;
    private readonly Transform _transform;

    public Vector3 Centre { get; private set; }
    private readonly Vector2 _startPercent, _endPercent;

    private MeshFilter _mf;
    private MeshRenderer _mr;
    private MeshCollider _mc;
    private Material _mat;

    public Vector2[] UVs = new Vector2[Size * Size];
    public Vector3[] Verts = new Vector3[Size * Size];
    public Vector3[] Norms = new Vector3[Size * Size];
    public Vector4[] Tangents = new Vector4[Size * Size];
    public int[] Triangles = new int[(Size - 1) * (Size - 1) * 6];

    public readonly int Depth;
    private readonly int _physicsLayer;

    public CubeSphereTile(Face face, Transform parent, int physicsLayer, int depth, Vector2 startPct, Vector2 endPct)
    {
        _physicsLayer = physicsLayer;
        _startPercent = startPct;
        _endPercent = endPct;

        _gameObject = new GameObject($"{face}, x: {startPct.x:N},{endPct.x:N}, y: {startPct.y:N},{endPct.y:N}");
        _transform = _gameObject.transform;

        _transform.parent = parent;
        _transform.localPosition = Vector3.zero;
        _transform.localRotation = Quaternion.identity;

        _face = face;
        Depth = depth;
    }

    /*
    private void Show(bool show)
    {
        _mr.enabled = show;

        foreach (var child in Children)
            child?.Show(false);
    }
    public void ShowOrBuildBest(Vector3 viewerPosition, Func<Vector3, float, float, float> noiseGenerator, float radius, float heightScaler, int allowedTimePerFrame)
    {
        const int width = 2;
        float closest = float.MaxValue;
        CubeSphereTile best = null;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < width; y++)
            {
                var idx = y * width + x;
                if (Children[idx] == null)
                {
                    var halfX = (_endPercent.x - _startPercent.x) / 2f;
                    var halfY = (_endPercent.y - _startPercent.y) / 2f;

                    var startX = _startPercent.x + (x * halfX);
                    var endX = _startPercent.x + halfX + (x * halfX);

                    var startY = _startPercent.y + (y * halfY);
                    var endY = _startPercent.y + halfY + (y * halfY);

                    var start = new Vector2(startX, startY);
                    var end = new Vector2(endX, endY);
                    var c = new CubeSphereTile(_face, _transform, _physicsLayer, Depth + 1, start, end);
                    c.GenerateData(noiseGenerator, radius, heightScaler);
                    c.SetMesh(c.MakeMesh(), _mat);
                    Children[idx] = c;
                    c.Show(false);
                }

                var child = Children[idx];
                var dist = Vector3.Distance(viewerPosition, child.Centre);
                if (dist < closest)
                {
                    closest = dist;
                    best = child;
                }
            }
        }

        // hide self
        Show(false);

        // show all children recursively
        foreach (var child in Children)
        {
            if (child != null)
            {
                if (child == best && Depth < 4) // todo: dynamic depth
                {
                    best.ShowOrBuildBest(viewerPosition, noiseGenerator, radius, heightScaler, allowedTimePerFrame);
                }
                else
                {
                    child.Show(true);
                }
            }
        }
    }
    */

    private void DefinePolygonRotations(out Vector3 x, out Vector3 y, out Vector3 c, out float v)
    {
        // These "magic numbers" define the rotational direction of each
        // polygon on a specific face.

        switch (_face)
        {
            case Face.Front:
            case Face.Back:
                x = Vector3.left; //-x
                y = Vector3.up; //+y
                c = -(x + y) + Vector3.forward;
                v = _face == Face.Front ? 1 : -1;
                break;

            case Face.Left:
            case Face.Right:
                x = Vector3.back; //-z
                y = Vector3.up; //+y
                c = -(x + y) + Vector3.left;
                v = _face == Face.Left ? 1 : -1;
                break;

            case Face.Top:
            case Face.Bottom:
                x = Vector3.left; //-x
                y = Vector3.back; //-z
                c = -(x + y) + Vector3.up;
                v = _face == Face.Top ? 1 : -1;
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    /*
    private static void GetVertAndNormal(out Vector3 vertex, out Vector3 normal)
    {
        // todo: ALL the data caching
        var neighbours = new Vector3[9];
        var n = 0;
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                var xPos = xPct * xDir * 2f;
                var yPos = yPct * yDir * 2f;
                neighbours[n++] = MathExtensions.ImprovedCubeSphereVertexDistribution(xPos + yPos + centre) * vertMultiplier;
            }
        }

        // To find the normal for vertex N, average the 
        // normals from the triangles it is part of
        //               0---1 
        //               |\  | \
        //               | \ |  \ 
        //               3---N---5
        //                \  |\  |
        //                 \ | \ |
        //                   7---8

        vertex = neighbours[4]; // 4 is our middle vertex, the one we're doing all this work *for*
        normal = Vector3.zero;

        // top left and top middle
        var v = neighbours[0] - vertex;
        var w = neighbours[1] - vertex;
        normal += new Vector3((v.y * w.z) - (v.z * w.y), (v.z * w.x) - (v.x * w.z), (v.z * w.y) - (v.y * w.x));
        // top and right
        v = w;
        w = neighbours[5] - vertex;
        normal += new Vector3((v.y * w.z) - (v.z * w.y), (v.z * w.x) - (v.x * w.z), (v.z * w.y) - (v.y * w.x));
        // right and bottom right
        v = w;
        w = neighbours[8] - vertex;
        normal += new Vector3((v.y * w.z) - (v.z * w.y), (v.z * w.x) - (v.x * w.z), (v.z * w.y) - (v.y * w.x));
        // bottom right and bottom
        v = w;
        w = neighbours[7] - vertex;
        normal += new Vector3((v.y * w.z) - (v.z * w.y), (v.z * w.x) - (v.x * w.z), (v.z * w.y) - (v.y * w.x));
        // bottom and left
        v = w;
        w = neighbours[3] - vertex;
        normal += new Vector3((v.y * w.z) - (v.z * w.y), (v.z * w.x) - (v.x * w.z), (v.z * w.y) - (v.y * w.x));
        // left and top left
        v = w;
        w = neighbours[0] - vertex;
        normal += new Vector3((v.y * w.z) - (v.z * w.y), (v.z * w.x) - (v.x * w.z), (v.z * w.y) - (v.y * w.x));

        // sum the face normals and normalize the output.
        normal.Normalize();
    }
    */

    public void GenerateData(Func<Vector3, float, float, float> noiseGenerator, float radius, float heightScaler)
    {
        Vector3 xDir;
        Vector3 yDir;
        Vector3 centre;
        float vertMultiplier;

        DefinePolygonRotations(out xDir, out yDir, out centre, out vertMultiplier);

        bool clockwise = vertMultiplier < 0;

        var min = Vector3.one * float.MaxValue;
        var max = Vector3.one * float.MinValue;

        int vertexIndex = 0;
        int triangleIndex = 0;
        for (int x = 0; x < Size; x++)
        {
            var xScale = x / (Size - 1f);
            var xPct = Mathf.Lerp(_startPercent.x, _endPercent.x, xScale);

            for (int y = 0; y < Size; y++, vertexIndex++)
            {
                var yScale = y / (Size - 1f);
                var yPct = Mathf.Lerp(_startPercent.y, _endPercent.y, yScale);

                var xPos = xPct * xDir * 2f;
                var yPos = yPct * yDir * 2f;

                var vertex = MathExtensions.ImprovedCubeSphereVertexDistribution(xPos + yPos + centre) * vertMultiplier;
                var vn = vertex.normalized;

                var t = Vector3.Cross(vn, yDir);
                Tangents[vertexIndex] = new Vector4(t.x, t.y, t.z, 1f);

                Norms[vertexIndex] = vn;
                UVs[vertexIndex] = MathExtensions.Vector3ToSphericalUV(vn);

                var noise = noiseGenerator(vn, xPct, yPct);
                var rad = radius + (radius * (noise * heightScaler));
                var v = vertex * rad;
                Verts[vertexIndex] = v;

                min.x = Mathf.Min(min.x, v.x);
                min.y = Mathf.Min(min.y, v.y);
                min.z = Mathf.Min(min.z, v.z);
                max.x = Mathf.Max(max.x, v.x);
                max.y = Mathf.Max(max.y, v.y);
                max.z = Mathf.Max(max.z, v.z);

                if (x < (Size - 1) && y < (Size - 1))
                {
                    var a = vertexIndex;
                    var b = vertexIndex + 1;
                    var c = vertexIndex + Size;
                    var d = vertexIndex + Size + 1;

                    if (clockwise)
                    {
                        // clockwise
                        // a, c, d
                        // b, d, a
                        // a ----- b
                        // | \     |
                        // |   \   |
                        // |     \ |
                        // c ----- d

                        Triangles[triangleIndex] = a;
                        Triangles[triangleIndex + 1] = c;
                        Triangles[triangleIndex + 2] = d;
                        Triangles[triangleIndex + 3] = d;
                        Triangles[triangleIndex + 4] = b;
                        Triangles[triangleIndex + 5] = a;
                        triangleIndex += 6;
                    }
                    else
                    {
                        // counterclockwise
                        // a, b, d
                        // b, c, a
                        // a ----- b
                        // | \     |
                        // |   \   |
                        // |     \ |
                        // c ----- d

                        Triangles[triangleIndex] = a;
                        Triangles[triangleIndex + 1] = b;
                        Triangles[triangleIndex + 2] = d;
                        Triangles[triangleIndex + 3] = d;
                        Triangles[triangleIndex + 4] = c;
                        Triangles[triangleIndex + 5] = a;
                        triangleIndex += 6;
                    }
                }
            }
        }

        //CalculateTangents(ref verts, ref norms, ref triangles, ref uvs, ref tangents);

        Centre = ((max - min) / 2f) + min;
    }

    private static void CalculateNormals(ref Vector3[] verts, ref Vector3[] norms, ref int[] triangles)
    {
        // todo
    }

    /// <summary>
    /// CalculateTangents converted to C# by Geoffrey 'Grey' Clark, original reference:
    /// Lengyel, Eric. “Computing Tangent Space Basis Vectors for an Arbitrary Mesh”. Terathon Software, 2001. http://terathon.com/code/tangent.html
    /// </summary>
    private static void CalculateTangents(ref Vector3[] verts, ref Vector3[] norms, ref int[] triangles, ref Vector2[] uvs, ref Vector4[] tangents)
    {
        var vertexCount = verts.Length;
        var triangleCount = triangles.Length;

        Vector3[] tan1 = new Vector3[vertexCount * 2];
        Vector3[] tan2 = new Vector3[vertexCount * 3];

        for (int i = 0; i < triangleCount; i += 3)
        {
            var idx1 = triangles[i];
            var idx2 = triangles[i + 1];
            var idx3 = triangles[i + 2];

            Vector3 v1 = verts[idx1];
            Vector3 v2 = verts[idx2];
            Vector3 v3 = verts[idx3];

            Vector2 w1 = uvs[idx1];
            Vector2 w2 = uvs[idx2];
            Vector2 w3 = uvs[idx3];

            float x1 = v2.x - v1.x;
            float x2 = v3.x - v1.x;
            float y1 = v2.y - v1.y;
            float y2 = v3.y - v1.y;
            float z1 = v2.z - v1.z;
            float z2 = v3.z - v1.z;

            float s1 = w2.x - w1.x;
            float s2 = w3.x - w1.x;
            float t1 = w2.y - w1.y;
            float t2 = w3.y - w1.y;

            var r = 1.0f / (s1 * t2 - s2 * t1);
            var sdir = new Vector3((t2 * x1 - t1 * x2) * r,
                                   (t2 * y1 - t1 * y2) * r,
                                   (t2 * z1 - t1 * z2) * r);

            var tdir = new Vector3((s1 * x2 - s2 * x1) * r,
                                   (s1 * y2 - s2 * y1) * r,
                                   (s1 * z2 - s2 * z1) * r);

            tan1[idx1] += sdir;
            tan1[idx2] += sdir;
            tan1[idx3] += sdir;

            tan2[idx1] += tdir;
            tan2[idx2] += tdir;
            tan2[idx3] += tdir;
        }

        for (int i = 0; i < vertexCount; i++)
        {
            Vector3 n = norms[i];
            Vector3 t = tan1[i];

            // Gram-Schmidt orthogonalize
            tangents[i] = (t - n * Vector3.Dot(n, t)).normalized;

            // Calculate handedness
            tangents[i].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[i]) < 0.0F) ? -1.0f : 1.0f;
        }
    }

    private Mesh _mesh;

    public Mesh MakeMesh()
    {
        _mesh = new Mesh
                {
                    name = _gameObject.name,
                    vertices = Verts,
                    triangles = Triangles,
                    uv = UVs,
                };

        // due to internal unity "stuff" ensure that the 
        // normals are assigned BEFORE assigning the tangents.
        _mesh.normals = Norms;

        // recalculating the normals here does offer a significant
        // improvement in shading. I need to research what's being
        // done and see if I can replicate it in my own normal 
        // generation step.
        _mesh.RecalculateNormals();
        var newNorms = _mesh.normals;

        // recalculating the tangents here provides improved output
        // due to them taking whole triangles into account.
        CalculateTangents(ref Verts, ref newNorms, ref Triangles, ref UVs, ref Tangents); // this can be offloaded to a thread eventually
        _mesh.tangents = Tangents;

        _mesh.RecalculateBounds();
        return _mesh;
    }

    public void SetMesh(Mesh mesh, Material mat)
    {
        if (_mf == null) _mf = _gameObject.AddComponent<MeshFilter>();
        _mf.sharedMesh = mesh;

        if (_mr == null) _mr = _gameObject.AddComponent<MeshRenderer>();
        _mr.sharedMaterial = mat;

        if (!DebuggingMess.Skip_MeshColliderGeneration)
        {
            if (_mc == null) _mc = _gameObject.AddComponent<MeshCollider>();
            _mc.sharedMesh = mesh;
        }

        _mat = mat;
    }
}
